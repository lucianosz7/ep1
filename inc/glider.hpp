#ifndef GLIDER_HPP
#define GLIDER_HPP

#include "forma.hpp"

class Glider : public Forma{
	public:
		Glider();
		Glider(int linha,int coluna);
};
#endif
