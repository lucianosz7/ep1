#ifndef BLINKER_HPP
#define BLINKER_HPP

#include "forma.hpp"

class Blinker : public Forma{
	public:
		Blinker();
		Blinker(int linha,int coluna);
};
#endif
