#ifndef FORMA_HPP
#define FORMA_HPP

using namespace std;

class Forma{
	private:
		char campo[30][50];
		int vizinhos[30][50];
	public:
		Forma();
		char getLocalizacao(int linha,int coluna);
		void setLocalizacao(int linha,int coluna,char viva);

		int get_vizinhos(int linha,int coluna);
		void set_vizinhos();
		void regras();
		void imprimir();
};
#endif
