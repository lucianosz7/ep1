#include<bits/stdc++.h>
#include "usuario.hpp"
#include "forma.hpp"
#include "block.hpp"
#include "blinker.hpp"
#include "glider.hpp"
#include "gun.hpp"

using namespace std;
int menu_menu()
{
	int opt;
	cout << "******************MENU******************" <<endl;
	cout << "*********CONWAY'S GAME OF LIFE**********" <<endl;
	cout << "|1.Automático                          |" << endl;
	cout << "|2.Manual                              |" << endl;
	cout << "|                                      |"  << endl;
	cout << "|                                      |" << endl;
	cout << "|                                      |" << endl;
	cout << "******************FIM******************" << endl;
	cin >> opt;
	if(opt != 1 &&  opt != 2)
	{
		cout << "Opção invalida!!" << endl;
		cout << "Digite novamente: " ;
		cin >> opt;
		return opt;
	}
	if(opt == 1 || opt == 2)
	{
		return opt;
	}
}

int menu_menu_gun()
{
	int opt;
	cout << "******************MENU******************" <<endl;
	cout << "*********CONWAY'S GAME OF LIFE**********" <<endl;
	cout << "|1.Automático                          |" << endl;
	cout << "|                                      |" << endl;
	cout << "|                                      |"  << endl;
	cout << "|                                      |" << endl;
	cout << "|                                      |" << endl;
	cout << "******************FIM******************" << endl;
	cin >> opt;
	if(opt != 1)
	{
		cout << "Opção invalida!!" << endl;
		cout << "Digite novamente: " ;
		cin >> opt;
		return opt;
	}
	if(opt == 1)
	{
		return opt;
	}
}


int  menu()
{
	int opt;
	cout << "******************MENU******************" <<endl;
	cout << "*********CONWAY'S GAME OF LIFE**********" <<endl;
	cout << "|1.BLOCK                               |" << endl;
	cout << "|2.BLIMKER                             |" << endl;
	cout << "|3.GLIDER                              |"  << endl;
	cout << "|4.GOSPER GLIDER GUN                   |" << endl;
	cout << "|5.SAIR                                |" << endl;
	cout << "******************FIM*******************" << endl;
	cin >> opt;
	
	return opt;
}
int escolha(int opt)
{
	Block block;
	Blinker blinker;
	Glider glider;
	Gun gun;
	
	switch(opt){
		case 1:
			
			opt = menu_menu();
			if(opt == 1){
				block.imprimir();
			}
			if(opt == 2)
			{
				int linha,coluna;
				cout << "Digite a posição: ";
				cin >> linha ;
				cin >> coluna;
				Block block(linha,coluna);
				block.imprimir();
			}

			break;
		case 2:
			
			opt = menu_menu();
			if(opt == 1){
				blinker.imprimir();
			}
			if(opt == 2)
			{
				int linha,coluna;
				cout << "Digite a posição: ";
				cin >> linha ;
				cin >> coluna;
				Blinker blinker(linha,coluna);
				blinker.imprimir();
			}
			
			break;
		case 3:
			
			opt = menu_menu();
			if(opt == 1){
				glider.imprimir();
			}
			if(opt == 2)
			{
				int linha,coluna;
				cout << "Digite a posição: ";
				cin >> linha ;
				cin >> coluna;
				Glider glider(linha,coluna);
				glider.imprimir();
			}

			
			break;
		case 4:

			
			opt = menu_menu_gun();
			if(opt == 1){
				gun.imprimir();
			}
			
			break;
		case 5:
			exit(0);
		default:
			cout << "Opção incorreta!" << endl;
			cout << "Fim de Operação" << endl;
			

		
	}
	return opt;
}
int main(int argc, char ** argv)
{
	int opcao;
	opcao = menu();
	escolha(opcao);

	return 0;
}
				
