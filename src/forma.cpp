#include<iostream>
#include "forma.hpp"
#include <unistd.h>

using namespace std;

Forma::Forma(){
	int i,j;
	for(i=0;i<30;i++)
	{
		for(j=0;j < 50;j++){
			campo[i][j] = ' ';
		}
	}
}
char Forma::getLocalizacao(int linha,int coluna){
	return campo[linha][coluna];
}
void Forma::setLocalizacao(int linha,int coluna,char viva){
	campo[linha][coluna] = viva;
}

int Forma::get_vizinhos(int linha,int coluna){
	return vizinhos[linha][coluna];
}
void Forma::set_vizinhos(){
	int n_vizinhos=0,linha,coluna;

	for(linha=0;linha<30;linha++)
	{
		for(coluna=0;coluna<50;coluna++)
		{
			if(getLocalizacao(linha+1,coluna+1) == 'o')
			{
				n_vizinhos++;
			}
			if(getLocalizacao(linha+1,coluna) == 'o')
			{
				n_vizinhos++;
			}
			if(getLocalizacao(linha,coluna+1) == 'o')
			{
				n_vizinhos++;
			}
			if(getLocalizacao(linha-1,coluna-1) == 'o')
			{
				n_vizinhos++;
			}
			if(getLocalizacao(linha-1,coluna) == 'o')
			{
				n_vizinhos++;
			}
			if(getLocalizacao(linha,coluna-1) == 'o')
			{
				n_vizinhos++;
	
			}
			if(getLocalizacao(linha+1,coluna-1) == 'o')
			{
				n_vizinhos++;
			}
			if(getLocalizacao(linha-1,coluna+1) == 'o')
			{
				n_vizinhos++;
			}
			vizinhos[linha][coluna] = n_vizinhos;
		      	n_vizinhos = 0;	
		}
	}
			
}
void Forma::regras(){
	set_vizinhos();
	int linha,coluna;
	for(linha=0;linha<30;linha++)
	{
		for(coluna=0;coluna < 50;coluna++)
		{
			if(get_vizinhos(linha,coluna) <= 1)
			{
				setLocalizacao(linha,coluna,' ');
			}	
			if(get_vizinhos(linha,coluna) >= 4)
			{
				setLocalizacao(linha,coluna,' ');
			}
			if(getLocalizacao(linha,coluna) == ' ' && get_vizinhos(linha,coluna) == 3){
				setLocalizacao(linha,coluna,'o');
			}
			if(get_vizinhos(linha,coluna) == 'o' && get_vizinhos(linha,coluna) == 3)
			{
				setLocalizacao(linha,coluna,'o');
			}
		}
	}
}
void Forma::imprimir(){
	int linha,coluna,geracao;
	
	for(geracao=0;geracao<100;geracao++){
		system("clear");
		for(linha=0;linha < 30;linha++)
		{
			cout << "+";
			for(coluna=0;coluna < 50;coluna++)
			{
				cout << getLocalizacao(linha,coluna);
			}
			cout << "+" << endl;
		}
		regras();
		usleep(200000);
		

	}
	

}
